# Problems with JAXWS generated code in IntelliJ using JDK 1.6

This repository contains an example project that demonstrates problems that
IntelliJ has with code generated with `jaxws-maven-plugin` when using JDK 1.6.

Note that `jaxws-maven-plugin` has been configured in `pom.xml` according to
the [official instructions](https://jax-ws-commons.java.net/jaxws-maven-plugin/usage.html).

Given JDK version `1.6.0_45` and IntelliJ version `14.1.3`,  
when I try to compile the project in this directory with IntelliJ  
then I receive the following error:  

    Error:(46, 9) java: C:\devel\other\tmp\broken\broken-app\target\generated-sources\xjc\com\cydne\ws\Weather.java:46: cannot find symbol
    symbol  : constructor Service(java.net.URL,javax.xml.namespace.QName,javax.xml.ws.WebServiceFeature[])
    location: class javax.xml.ws.Service

However, compiling with Maven works:

    $ mvn clean compile
    [INFO] Scanning for projects...
    [WARNING]
    [WARNING] Some problems were encountered while building the effective model for com.broken.app:broken-app:jar:1.0-SNAPSH
    OT
    [WARNING] 'build.plugins.plugin.version' for org.apache.maven.plugins:maven-compiler-plugin is missing. @ line 33, colum
    n 12
    [WARNING]
    [WARNING] It is highly recommended to fix these problems because they threaten the stability of your build.
    [WARNING]
    [WARNING] For this reason, future Maven versions might no longer support building such malformed projects.
    [WARNING]
    [INFO]
    [INFO] ------------------------------------------------------------------------
    [INFO] Building broken-app 1.0-SNAPSHOT
    [INFO] ------------------------------------------------------------------------
    [INFO]
    [INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ broken-app ---
    [INFO] Deleting c:\devel\other\tmp\broken\broken-app\target
    [INFO]
    [INFO] --- maven-dependency-plugin:2.3:copy (default) @ broken-app ---
    [INFO]
    [INFO] --- jaxws-maven-plugin:2.3:wsimport (default) @ broken-app ---
    [WARNING] cannot find file for junit:junit
    [INFO] Processing: file:/c:/devel/other/tmp/broken/broken-app/src/main/resources/weather.wsdl
    [WARNING] Using platform encoding (Cp1257), build is platform dependent!
    [INFO] jaxws:wsimport args: [-keep, -s, c:\devel\other\tmp\broken\broken-app\target\generated-sources\xjc, -d, c:\devel\
    other\tmp\broken\broken-app\target\classes, -extension, -Xnocompile, -p, com.cydne.ws, "file:/c:/devel/other/tmp/broken/
    broken-app/src/main/resources/weather.wsdl"]
    parsing WSDL...


    [WARNING] SOAP port "WeatherSoap12": uses a non-standard SOAP 1.2 binding.
      line 339 of file:/c:/devel/other/tmp/broken/broken-app/src/main/resources/weather.wsdl

    [WARNING] Port "WeatherHttpGet" is not a SOAP port, it has no soap:address
      line 342 of file:/c:/devel/other/tmp/broken/broken-app/src/main/resources/weather.wsdl

    [WARNING] port "WeatherHttpGet": not a standard SOAP port. The generated artifacts may not work with JAX-WS runtime.
      line 342 of file:/c:/devel/other/tmp/broken/broken-app/src/main/resources/weather.wsdl

    [WARNING] Port "WeatherHttpPost" is not a SOAP port, it has no soap:address
      line 345 of file:/c:/devel/other/tmp/broken/broken-app/src/main/resources/weather.wsdl

    [WARNING] port "WeatherHttpPost": not a standard SOAP port. The generated artifacts may not work with JAX-WS runtime.
      line 345 of file:/c:/devel/other/tmp/broken/broken-app/src/main/resources/weather.wsdl


    Generating code...

    [INFO]
    [INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ broken-app ---
    [WARNING] Using platform encoding (Cp1257 actually) to copy filtered resources, i.e. build is platform dependent!
    [INFO] Copying 1 resource
    [INFO]
    [INFO] --- maven-compiler-plugin:3.1:compile (default-compile) @ broken-app ---
    [INFO] Changes detected - recompiling the module!
    [WARNING] File encoding has not been set, using platform encoding Cp1257, i.e. build is platform dependent!
    [INFO] Compiling 21 source files to c:\devel\other\tmp\broken\broken-app\target\classes
    [INFO] ------------------------------------------------------------------------
    [INFO] BUILD SUCCESS
    [INFO] ------------------------------------------------------------------------
    [INFO] Total time: 5.910 s
    [INFO] Finished at: 2015-05-25T21:28:09+03:00
    [INFO] Final Memory: 13M/243M
    [INFO] ------------------------------------------------------------------------